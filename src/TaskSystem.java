import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TaskSystem {

    public List<Task> taskList;
    public List<User> userList;

    public TaskSystem() {
        this.taskList = new ArrayList<>();
        this.userList = new ArrayList<>();
    }

    public void createTask(Task task) {
        taskList.add(task);
    }

    public void createUser(User user) {
        userList.add(user);
    }

    public void assignTask(Task task, User user) {
        task.setAssignment(user.getName());
    }

    public void changeStarDate(Task task, String date) {
        task.setStarDate(date);
    }

    public void changeEndDate(Task task, String date) {
        task.setEndDate(date);
    }

    public void changeDescription(Task task, String description) {
        task.setDescription(description);
    }

    public void changeStatus(Task task, String status) {
        task.setStatus(status);
    }

    public User getUserById(int userId) {
        for (User user : userList) {
            if (user.getId() == userId) {
                return user;
            }


        }
        return userList.isEmpty() ? null : userList.get(0);
    }
    public Task getTaskById(int taskId) {
        for (Task task : taskList) {
            if (task.getId() == taskId) {
                return task;
            }


        }
        return taskList.isEmpty() ? null : taskList.get(0);
    }
}