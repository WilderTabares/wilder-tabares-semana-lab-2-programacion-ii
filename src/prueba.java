import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class Tarea {
    private String descripcion;
    private Usuario asignado;
    private int nivelPrioridad;

    public Tarea(String descripcion, Usuario asignado, int nivelPrioridad) {
        this.descripcion = descripcion;
        this.asignado = asignado;
        this.nivelPrioridad = nivelPrioridad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public Usuario getAsignado() {
        return asignado;
    }

    public int getNivelPrioridad() {
        return nivelPrioridad;
    }
}

class Usuario {
    private String nombre;
    private List<Tarea> tareasAsignadas;

    public Usuario(String nombre) {
        this.nombre = nombre;
        this.tareasAsignadas = new ArrayList<>();
    }

    public String getNombre() {
        return nombre;
    }

    public List<Tarea> getTareasAsignadas() {
        return tareasAsignadas;
    }

    public void agregarTarea(Tarea tarea) {
        tareasAsignadas.add(tarea);
    }
}

class SistemaGestionTareas {
    private List<Tarea> listaTareas;
    private List<Usuario> listaUsuarios;

    public SistemaGestionTareas() {
        this.listaTareas = new ArrayList<>();
        this.listaUsuarios = new ArrayList<>();
    }

    public void crearTarea(String descripcion, Usuario asignado, int nivelPrioridad) {
        Tarea nuevaTarea = new Tarea(descripcion, asignado, nivelPrioridad);
        listaTareas.add(nuevaTarea);
        asignado.agregarTarea(nuevaTarea);
    }

    public void agregarUsuario(Usuario usuario) {
        listaUsuarios.add(usuario);
    }
}



public class prueba {
    public static void main(String[] args) {
        // Crear sistema de gestión de tareas
        SistemaGestionTareas sistema = new SistemaGestionTareas();

        // Solicitar información del usuario
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese el nombre del Usuario 1: ");
        String nombreUsuario1 = scanner.nextLine();
        Usuario usuario1 = new Usuario(nombreUsuario1);
        sistema.agregarUsuario(usuario1);

        System.out.print("Ingrese el nombre del Usuario 2: ");
        String nombreUsuario2 = scanner.nextLine();
        Usuario usuario2 = new Usuario(nombreUsuario2);
        sistema.agregarUsuario(usuario2);

        // Solicitar información de la tarea
        System.out.print("Ingrese la descripción de la Tarea 1: ");
        String descripcionTarea1 = scanner.nextLine();
        System.out.print("Ingrese la prioridad de la Tarea 1 (entero): ");
        int prioridadTarea1 = scanner.nextInt();
        scanner.nextLine(); // Consumir la nueva línea

        // Crear y asignar tarea 1
        sistema.crearTarea(descripcionTarea1, usuario1, prioridadTarea1);

        // Solicitar información de la tarea
        System.out.print("Ingrese la descripción de la Tarea 2: ");
        String descripcionTarea2 = scanner.nextLine();
        System.out.print("Ingrese la prioridad de la Tarea 2 (entero): ");
        int prioridadTarea2 = scanner.nextInt();
        scanner.nextLine(); // Consumir la nueva línea

        // Crear y asignar tarea 2
        sistema.crearTarea(descripcionTarea2, usuario2, prioridadTarea2);

        // Imprimir tareas asignadas a cada usuario
        System.out.println("\nTareas asignadas a " + usuario1.getNombre() + ":");
        for (Tarea tarea : usuario1.getTareasAsignadas()) {
            System.out.println("- " + tarea.getDescripcion() + " (Prioridad: " + tarea.getNivelPrioridad() + ")");
        }

        System.out.println("\nTareas asignadas a " + usuario2.getNombre() + ":");
        for (Tarea tarea : usuario2.getTareasAsignadas()) {
            System.out.println("- " + tarea.getDescripcion() + " (Prioridad: " + tarea.getNivelPrioridad() + ")");
        }

        // Cerrar el scanner
        scanner.close();
    }
}


