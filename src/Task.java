import java.util.*;

public class Task {

    private int id;
    private String name;
    private String description;
    private  String starDate;
    private  String endDate;
    private String priority;
    private String assignment;
    private String Status;

    public Task(int id, String name, String description, String starDate, String endDate, String priority, String assignment, String status) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.starDate = starDate;
        this.endDate = endDate;
        this.priority = priority;
        this.assignment = assignment;
        this.Status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStarDate() {
        return starDate;
    }

    public void setStarDate(String starDate) {
        this.starDate = starDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getAssignment() {
        return assignment;
    }

    public void setAssignment(String assignment) {
        this.assignment = assignment;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }
}