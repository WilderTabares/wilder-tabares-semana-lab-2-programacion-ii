import java.util.Date;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        TaskSystem monky = new TaskSystem();
        int option;
        boolean flag = false;
        System.out.println("*********** Welcome to the ArMonky ***********");
        System.out.println("             Your task manager                ");
        System.out.println("         Let's start with a test...           ");
        System.out.println("        Let´s start with making a user:         ");
        User user = fillUserData();

        System.out.println("        Let´s start with making a Task:         ");
        Task task = fillTaskData();

        System.out.println("        Now, What do you want to do?:         ");

        while (flag != true ){

            System.out.println("*********** Welcome to the ArMonky ***********");
            System.out.println("1. Create a new task.\n2. Create a new user.\n3. Assign task.\n4. Modify description.\n5. Modify other things.\n6. Print.\n7. Exit");

            option = scanner.nextInt();

            switch (option){

                case 1:
                    System.out.println("------------ Task Creator ------------");
                    Task newTask = fillTaskData();
                    monky.createTask(task);

                    break;

                case 2:
                    System.out.println("------------ User Creator ------------");
                    User newUser = fillUserData();
                    monky.createUser(user);
                    break;
                case 3:
                    System.out.println("------------ Assign task ------------");

                    System.out.println("Enter Task ID to assign: ");
                    int taskIdToAssign = scanner.nextInt();
                    System.out.println("Enter User ID to assign: ");
                    int userIdToAssign = scanner.nextInt();

                    Task taskToAssign = monky.getTaskById(taskIdToAssign);
                    User userToAssign = monky.getUserById(userIdToAssign);
                    monky.assignTask(taskToAssign,userToAssign);
                    break;
                case 4:
                    System.out.println("------------ Modify Description ------------");

                    System.out.println("Enter Task ID to change: ");
                    int taskId = scanner.nextInt();
                    System.out.println("Enter the new description: ");
                    Task taskToDescrition = monky.getTaskById(taskId);

                    String newDescription = scanner.nextLine();
                    monky.changeDescription(taskToDescrition, newDescription);
                    break;
                case 5:
                    System.out.println("Coming Soon.....");
                    break;
                case 6:
                    System.out.println("-------------- Print Tasks and Users --------------");


                    System.out.println("All Task:");
                    for (Task taskp : monky.taskList) {
                        System.out.println(taskp);
                    }
                    System.out.println();

                    System.out.println("All Users:");
                    for (User userp : monky.userList) {
                        System.out.println(userp);
                    }
                    System.out.println();
                    break;
                case 7:
                    System.out.println("Bye Bye:D");
                    flag=true;

                    break;

            }



        }




    }
    public static Task fillTaskData(){
        Scanner scanner = new Scanner(System.in);

        System.out.print("ID: ");
        int id = scanner.nextInt();
        scanner.nextLine();

        System.out.print("Name of the task: ");
        String name = scanner.nextLine();

        System.out.print("Description: ");
        String description = scanner.nextLine();

        System.out.print("Start Date (yyyy-MM-dd): ");
        String startDate = scanner.nextLine();

        System.out.print("End Date (yyyy-MM-dd): ");
        String endDate = scanner.nextLine();

        System.out.print("Priority: ");
        String priority = scanner.nextLine();

        System.out.print("Assignment: ");
        String assignment = scanner.nextLine();

        System.out.print("Status: ");
        String status = scanner.nextLine();

        return new Task(id, name, description, startDate,endDate, priority, assignment,status);
    }
    public static User fillUserData(){
        Scanner scanner = new Scanner(System.in);

        System.out.print("ID: ");
        int id = scanner.nextInt();

        // Consumir la nueva línea pendiente después de nextInt
        scanner.nextLine();

        System.out.print("Name: ");
        String name = scanner.nextLine();

        System.out.print("Email: ");
        String email = scanner.nextLine();

        System.out.print("Number Phone: ");
        int numberPhone = scanner.nextInt();

        return new User(id, name, email, numberPhone);
    }
}
